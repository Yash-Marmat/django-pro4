-continution of project3
-providing login and logout privileges to the user so that there is no need to go to admin page
 again and again.
-here we created a seperate url for account web page that controls the login privileges, 
 the url is mention in project url file.
-created another folder inside templates folder called registration (contains login.html file).
-format followed: urls (project), registration (login.html), LOGIN_REDIRECT_URL (in settings).
-similarly for logout-> LOGOUT_REDIRECT_URL = 'home' (in settings)

-NOTE: in this project i created two apps one is 'blog' app controlling the log in and log out
       and other app 'accounts' for controlling sign up privileges.
       