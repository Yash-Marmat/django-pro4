from django.urls import path
from .views import SignUpBlog

urlpatterns = [
    path('signup/', SignUpBlog.as_view(), name = 'signup'),
]