from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic


class SignUpBlog(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

'''
Summary

We need to write our own view for a sign up page to register new users, but Django
provides us with a form class, UserCreationForm, to make things easier. By default it
comes with three fields: username, passwordǐ, and passwordǑ.

We’re subclassing the generic class-based view CreateView in our SignUpView class. We
specify the use of the built-in UserCreationForm. we use reverse_lazy to redirect the 
user to the log in page upon successful registration.
Why use reverse_lazy here instead of reverse? 
The reason is that for all generic classbased views the URLs are not loaded when the
file is imported, so we have to use the lazy form of reverse to load them later when 
they’re available.
'''